package com.choucair.utils;

public class Utils {
	
	public static double stringWithLettersToNumber(String number) {
		StringBuilder numberToSendBuilder = new StringBuilder();
		for (int i = 0; i < number.length(); i++) {
			if (isNumeric("" + number.charAt(i))) {
				numberToSendBuilder.append(number.charAt(i));
			}
			else {
				break;
			}
		}
		return Double.parseDouble(numberToSendBuilder.toString());
	}

	
	public static boolean isNumeric(String str) { 
		try {  
			if (str.equals(".")) {
				return true;
			}
			Double.parseDouble(str);  
			return true;
		}catch(NumberFormatException e){  
			return false;  
	  }  
	}
}
