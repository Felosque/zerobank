package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.AccountActivitySteps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class AccountActivityDefinitions {
	
	@Steps
	private AccountActivitySteps accountActivitySteps;
	
	@Given("Me dirijo a la pagina de Account Activity")
	public void me_dirijo_a_la_pagina_de_account_activity() {
		accountActivitySteps.goTo_AccountActivity();
	}

	@Given("Selecciono la funcionalidad de Show Transactions")
	public void selecciono_la_funcionalidad_de_show_transactions() {
		accountActivitySteps.goTo_ShowTransaction();
	}

	@When("Selecciono la opcion {string} de la lista")
	public void selecciono_la_opcion_de_la_lista(String typeAccount) {
		accountActivitySteps.selectAccount(typeAccount);
	}

	@Then("El sistema muestra las transacciones de tipo Loan")
	public void el_sistema_muestra_la_transaccion_de_tipo_loan_con_fecha_de() {
		accountActivitySteps.checkTransactionDate();
	}
	
	@Then("El sistema muestra un mensaje de {string}")
	public void el_sistema_muestra_un_mensaje_de(String errorMesagge) {
		accountActivitySteps.checkMessageError(errorMesagge);
	}
	
	@Given("Selecciono la funcionalidad de Find Transactions")
	public void selecciono_la_funcionalidad_de_find_transactions() {
		accountActivitySteps.goTo_FindTransaction();
	}

	@When("ingreso los datos de busqueda")
	public void ingreso_los_datos_de_busqueda(DataTable dataTable) {
		List<List<String>> data = dataTable.asLists();
		for (int i = 1; i < data.size(); i++) {
			accountActivitySteps.fillFormFindTransactions(data, i);
		}
	}

	@Then("El sistema me muestra una transaccion con la descripcion {string}")
	public void el_sistema_me_muestra_una_transaccion_con_la_descripcion(String description) {
		accountActivitySteps.checkTransactionDescription(description);
	}

	@Then("El sistema me muestra un mensaje de error diciendo {string}")
	public void el_sistema_me_muestra_un_mensaje_de_error_diciendo(String string) {
		accountActivitySteps.checkMessageError(string);
	}
}
