package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.TransferFundSteps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class TransferFundDefinitions {

	@Steps
	TransferFundSteps transferFundSteps;
	
	@Given("Me dirijo a la pagina de Transfer Funds")
	public void me_dirijo_a_la_pagina_de_transfer_funds() {
		transferFundSteps.goTo_transferFund();		
	}

	@When("Ingreso los datos para realizar la transaccion")
	public void ingreso_los_datos_para_realizar_la_transaccion(DataTable dataTable) {
		List<List<String>> data = dataTable.asLists();
		for (int i = 1; i < data.size(); i++) {
			transferFundSteps.fillDataInToForm(data, i);
		}
	}

	@When("Confirmo la realizacion de la transaccion")
	public void confirmo_la_realizacion_de_la_transaccion() {
		transferFundSteps.acceptTransaction();
	}

	@Then("el sistema muestra el mensaje de transaccion confirmada")
	public void el_sistema_muestra_el_mensaje_de_transaccion_confirmada() {
		transferFundSteps.transactionSuccefully();
	}
	
	@Then("el sistema muestra un mensaje de error")
	public void el_sistema_muestra_un_mensaje_de_error() {
		transferFundSteps.transactionFail();
	}

	
}
