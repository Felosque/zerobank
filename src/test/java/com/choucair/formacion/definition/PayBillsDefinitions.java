package com.choucair.formacion.definition;

import java.util.Iterator;
import java.util.List;

import com.choucair.formacion.steps.PayBillsSteps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class PayBillsDefinitions {
	
	@Steps
	PayBillsSteps payBillsSteps;
	
	@Given("Me dirijo a la pagina de Pay Bills")
	public void me_dirijo_a_la_pagina_de_pay_bills() {
		payBillsSteps.goTo_PayBillsPage();
	}

	@Given("Selecciono la opcion de  Pay Saved Payee")
	public void selecciono_la_opcion_de_pay_saved_payee() {
		payBillsSteps.goTo_PaySavedPayee();
	}
	
	@When("Ingreso los datos para realizar la transaccion al beneficiario")
	public void ingreso_los_datos_para_realizar_la_transaccion_al_beneficiario(DataTable dataTable) {
		List<List<String>> data = dataTable.asLists();
		for (int i = 1; i < data.size(); i++) {
			payBillsSteps.fillDataFormPaySavedPayee(data, i);
		}
	}
	
	@When("Confirmo la realizacion de la transaccion al beneficiario")
	public void confirmo_la_realizacion_de_la_transaccion_al_beneficiario() {
		payBillsSteps.transactionConfirm();
	}

	@Then("el sistema muestra que la transaccion fue exitosa")
	public void el_sistema_muestra_que_la_transaccion_fue_exitosa() {
		payBillsSteps.checkSuccefullyTransaction();
	}
	
	@Then("el sistema muestra un mensaje de error.")
	public void el_sistema_muestra_un_mensaje_de_error() {
		payBillsSteps.checkFailTransaction();
	}
	
	@Given("Selecciono la opcion de  Add New Payee")
	public void selecciono_la_opcion_de_add_new_payee() {
		payBillsSteps.goTo_AddNewPayee();
	}

	@When("Ingreso los datos para agregar al nuevo beneficiario")
	public void ingreso_los_datos_para_realizar_la_transaccion(DataTable dataTable) {
		List<List<String>> data = dataTable.asLists();
		for (int i = 1; i < data.size(); i++) {
			payBillsSteps.fillDataFormAddNewPayee(data, i);
		}
	}

	@When("Confirmo agregar al beneficiario")
	public void confirmo_agregar_al_beneficiario() {
		payBillsSteps.addNewPayeeConfirm();
	}

	@Then("el sistema un mensaje que se agrego correctamente")
	public void el_sistema_un_mensaje_que_se_agrego_correctamente() {
		payBillsSteps.checkSuccefullyTransaction();
	}

	@Given("Selecciono la opcion de  Purchase Foreign Currency")
	public void selecciono_la_opcion_de_purchase_foreign_currency() {
		payBillsSteps.goTo_PurchaseForeignCurrency();
	}

	@When("Ingreso los datos para calcular la conversion")
	public void ingreso_los_datos_para_calcular_la_conversion(DataTable dataTable) {
		List<List<String>> data = dataTable.asLists();
		for (int i = 1; i < data.size(); i++) {
			payBillsSteps.fillaDataFormPurchaseForeignCurrency(data, i);
		}
	}

	@When("Le doy al boton de comprar moneda extranjera")
	public void le_doy_al_boton_de_comprar_moneda_extranjera() {
		payBillsSteps.purchaseCurrencyConfirm();
	}


}
