package com.choucair.formacion.definition;

import com.choucair.formacion.steps.LoginSteps;

import io.cucumber.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class LoginDefinitions {
	
	@Steps
	private LoginSteps loginSteps;
	
	@Given("yo me autentico  en Zero Bank con el usuario {string} y la contraseña {string}")
	public void yo_me_autentico_en_zero_bank_con_el_usuario_y_la_contraseña(String username, String password) {
		loginSteps.loguearse(username, password);
	}
	
}
