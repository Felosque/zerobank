package com.choucair.formacion.steps;

import static org.junit.Assert.assertEquals;

import com.choucair.formacion.pageobjects.LoginPage;

import net.thucydides.core.annotations.Step;

public class LoginSteps {
	
	private LoginPage loginPage;
	
	@Step
	public void loguearse(String username, String passwornd) {
		loginPage.open();
		loginPage.setUsername(username);
		loginPage.setPassword(passwornd);
		loginPage.signIn();
		assertEquals("Zero Bank", loginPage.getLblTitle());
	}

}
