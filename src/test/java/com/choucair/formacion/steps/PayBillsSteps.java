package com.choucair.formacion.steps;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.swing.text.DefaultEditorKit.CopyAction;

import com.choucair.formacion.pageobjects.PayBillsPage;
import com.choucair.utils.Utils;

import net.thucydides.core.annotations.Step;

public class PayBillsSteps {
	
	PayBillsPage payBillsPage;
	
	@Step
	public void goTo_PayBillsPage() {
		payBillsPage.click_PayBills();
	}
	
	@Step
	public void goTo_PaySavedPayee() {
		payBillsPage.click_btnSavedPayeePay();
	}
	
	@Step 
	public void goTo_AddNewPayee() {
		payBillsPage.click_TabNewPayee();
	}
	
	@Step
	public void goTo_PurchaseForeignCurrency() {
		payBillsPage.click_TabPurchaseForeignCurrency();
	}
	
	@Step
	public void fillDataFormPaySavedPayee(List<List<String>> data, int id) {
		payBillsPage.select_SavedPayee(data.get(id).get(0));
		payBillsPage.select_SavedAccount(data.get(id).get(1));
		String amount = (data.get(id).get(2) == null) ? "" : data.get(id).get(2);
		payBillsPage.setSavedAmount(amount);
		payBillsPage.setSavedDate(data.get(id).get(3));
		payBillsPage.setSavedDescription(data.get(id).get(4));
	}
	
	@Step
	public void fillDataFormAddNewPayee(List<List<String>> data, int id) {
		String name = (data.get(id).get(0) == null) ? "" : data.get(id).get(0);
		payBillsPage.setAddPayeeName(name);
		payBillsPage.setAddPayeeAddress(data.get(id).get(1));
		payBillsPage.setAddPayeeAccount(data.get(id).get(2));
		payBillsPage.setAddPayeeDetails(data.get(id).get(3));
	}
	
	@Step
	public void fillaDataFormPurchaseForeignCurrency(List<List<String>> data, int id) {
		payBillsPage.select_PurchaseCurrency(data.get(id).get(0));
		payBillsPage.setPurchaseCurrencyAmount(data.get(id).get(1));
		payBillsPage.click_convertToDollars();
	}
	
	@Step 
	public void purchaseCurrencyConfirm() {
		payBillsPage.click_PurchaseForeignCurrency();
	}
	
	@Step
	public void transactionConfirm() {
		payBillsPage.click_btnSavedPayeePay();
	}
	
	@Step
	public void addNewPayeeConfirm() {
		payBillsPage.click_AddNewPayee();
	}
	
	@Step 
	public void checkSuccefullyTransaction() {
		assertEquals(payBillsPage.isSuccesfullyTransaction(), true);
	}
	
	@Step 
	public void checkFailTransaction() {
		assertEquals(payBillsPage.isSuccesfullyTransaction(), false);
	}

}
