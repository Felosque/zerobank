package com.choucair.formacion.steps;

import static org.junit.Assert.assertEquals;

import java.util.List;

import com.choucair.formacion.pageobjects.AccountActivityPage;

import net.thucydides.core.annotations.Step;

public class AccountActivitySteps {
	
	private AccountActivityPage accountActivityPage;
	
	@Step
	public void goTo_AccountActivity() {
		accountActivityPage.click_accountActivity();
	}
	
	@Step
	public void goTo_ShowTransaction() {
		accountActivityPage.click_showTransactions();
	}
	
	@Step
	public void goTo_FindTransaction() {
		accountActivityPage.click_findTransactions();
	}
	
	@Step
	public void selectAccount(String pAccount) {
		accountActivityPage.select_Account(pAccount);
	}
	
	@Step
	public void checkTransactionDate() {
		assertEquals(accountActivityPage.isTableVisible(), false);
	}
	
	@Step
	public void checkMessageError(String message) {
		assertEquals(accountActivityPage.getMessageError(), message);
	}
	
	@Step
	public void fillFormFindTransactions(List<List<String>> data, int id) {
		accountActivityPage.setDescription(data.get(id).get(0));
		accountActivityPage.setFromDate(data.get(id).get(1));
		accountActivityPage.setToDate(data.get(id).get(2));
		accountActivityPage.setFromAmount(data.get(id).get(3));
		accountActivityPage.setToAmount(data.get(id).get(4));
		accountActivityPage.select_Type(data.get(id).get(5));
		accountActivityPage.click_find();
	}
	
	@Step
	public void checkTransactionDescription(String description) {
		assertEquals(accountActivityPage.getTransactionDescription(), description);
	}

}
