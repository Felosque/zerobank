package com.choucair.formacion.steps;

import static org.junit.Assert.assertEquals;

import java.util.List;

import com.choucair.formacion.pageobjects.TransferFundPage;

import net.thucydides.core.annotations.Step;

public class TransferFundSteps {

	
	private TransferFundPage transferFundPage;
	
	@Step
	public void goTo_transferFund() {
		transferFundPage.click_TransferFund();
	}
	
	@Step
	public void fillDataInToForm(List<List<String>> data, int id) {
		transferFundPage.select_FromAccount(data.get(id).get(0).trim());
		transferFundPage.select_ToAccount(data.get(id).get(1).trim());
		String amount = (data.get(id).get(2) == null) ? "" : data.get(id).get(2).trim();
		transferFundPage.setAmount(amount);
		transferFundPage.setDescription(data.get(id).get(3).trim());
		transferFundPage.click_Continue();
	}
	
	@Step
	public void acceptTransaction() {
		transferFundPage.click_Submit();
	}
	
	@Step
	public void transactionSuccefully() {
		assertEquals(transferFundPage.isAlertSuccesfullyVisible(), true);
	}
	
	@Step
	public void transactionFail() {
		assertEquals(transferFundPage.isAlertSuccesfullyVisible(), false);
	}
}
