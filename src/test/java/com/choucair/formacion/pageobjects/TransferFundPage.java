package com.choucair.formacion.pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class TransferFundPage extends PageObject {
	
	@FindBy(xpath = "//a[contains(text(),'Transfer Funds')]")
	private WebElementFacade btnTranferFunds;
	
	@FindBy(xpath = "//select[@id='tf_fromAccountId']//option")
	private List<WebElement> cbFromAccount;
	
	@FindBy(xpath = "//select[@id='tf_toAccountId']//option")
	private List<WebElement> cbToAccount;
	
	@FindBy(id = "tf_amount")
	private WebElementFacade txtAmount;
	
	@FindBy(id = "tf_description")
	private WebElementFacade txtDescription;
	
	@FindBy(id = "btn_submit")
	private WebElementFacade btnContinue;
	
	@FindBy(id = "btn_submit")
	private WebElementFacade btnSubmit;
	
	@FindBy(xpath = "//div[@class='alert alert-success']")
	private WebElementFacade alertSuccesfully;
	
	
	public void click_TransferFund() {
		btnTranferFunds.click();
	}
	
	public void click_Continue() {
		btnContinue.click();
	}
	
	public void click_Submit() {
		btnSubmit.click();
	}
	
	public void select_FromAccount(String pFromAccount) {
		for (WebElement option : cbFromAccount) {
		    if (option.getText().contains(pFromAccount)) {
		        option.click();
		        break;
		    }
		}
	}
	
	public void select_ToAccount(String pToAccount) {
		for (WebElement option : cbToAccount) {
		    if (option.getText().contains(pToAccount)) {
		        option.click();
		        break;
		    }
		}
	}
	
	public void setAmount(String pAmount) {
		txtAmount.sendKeys(pAmount);
	}
	
	public void setDescription(String pDesc) {
		txtDescription.sendKeys(pDesc);
	}
	
	public boolean isAlertSuccesfullyVisible() {
		return alertSuccesfully.isCurrentlyVisible();
	}

}
