package com.choucair.formacion.pageobjects;

import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://zero.webappsecurity.com/login.html")
public class LoginPage extends PageObject {
	
	
	@FindBy(id = "user_login")
	private WebElementFacade username;
	
	@FindBy(id = "user_password")
	private WebElementFacade password;
	
	@FindBy(xpath = "//input[@name='submit']")
	private WebElementFacade btnSignIn;
	
	@FindBy(xpath = "//a[@class='brand']")
	private WebElementFacade lblTitle;


	public void setUsername(String pUsername) {
		username.clear();
		username.click();
		username.sendKeys(pUsername);
	}

	public void setPassword(String pPassword) {
		password.clear();
		password.click();
		password.sendKeys(pPassword);
	}
	
	public String getLblTitle() {
		return lblTitle.getText();
	}

	public void signIn() {
		btnSignIn.click();
	}
	
	

}
