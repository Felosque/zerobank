package com.choucair.formacion.pageobjects;

import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class AccountActivityPage extends PageObject {
	
	@FindBy(xpath = "//a[contains(text(),'Account Activity')]")
	private WebElementFacade btnAccountActivity;
	
	@FindBy(xpath = "//a[contains(text(),'Show Transactions')]")
	private WebElementFacade btnShowTransactions;

	@FindBy(xpath = "//a[contains(text(),'Find Transactions')]")
	private WebElementFacade btnFindTransactions;
	
	@FindBy(id = "aa_accountId")
	private WebElementFacade cbAccount;
	
	@FindBy(xpath = "///th[contains(text(),'Date')]")
	private WebElementFacade tableResults;
	
	@FindBy(xpath = "//div[@class='well']")
	private WebElementFacade messageError;
	
	@FindBy(id = "aa_description")
	private WebElementFacade description;
	
	@FindBy(id = "aa_fromDate")
	private WebElementFacade fromDate;
	
	@FindBy(id = "aa_toDate")
	private WebElementFacade toDate;
	
	@FindBy(id = "aa_fromAmount")
	private WebElementFacade fromAmount;
	
	@FindBy(id = "aa_toAmount")
	private WebElementFacade toAmount;
	
	@FindBy(id = "aa_type")
	private WebElementFacade cbType;
	
	@FindBy(xpath = "//button[@class='btn btn-primary']")
	private WebElementFacade btnFind;
	
	@FindBy(xpath = "//div[@id='filtered_transactions_for_account']//td[contains(text(),'ONLINE TRANSFER REF #UKKSDRQG6L')]")
	private WebElementFacade descTransaction;
	
	public void click_find() {
		btnFind.click();
	}
	
	public void click_accountActivity() {
		btnAccountActivity.click();
	}
	
	public void click_showTransactions() {
		btnShowTransactions.click();
	}
	
	public void click_findTransactions() {
		btnFindTransactions.click();
	}
	
	public void select_Account(String pText) {
		cbAccount.click();
		cbAccount.selectByVisibleText(pText);
		cbAccount.click();
	}
	
	public void select_Type(String pText) {
		cbType.click();
		cbType.selectByVisibleText(pText);
		cbType.click();
	}
	
	public String getMessageError() {
		return messageError.getText().trim();
	}
	
	public boolean isTableVisible() {
		return tableResults.isCurrentlyVisible();
	}

	public void setDescription(String pDescription) {
		description.clear();
		description.click();
		description.sendKeys(pDescription);
	}

	public void setFromDate(String pFromDate) {
		fromDate.clear();
		fromDate.click();
		fromDate.sendKeys(pFromDate);
	}

	public void setToDate(String pToDate) {
		toDate.clear();
		toDate.click();
		toDate.sendKeys(pToDate);
	}

	public void setFromAmount(String pFromAmount) {
		fromAmount.clear();
		fromAmount.click();
		fromAmount.sendKeys(pFromAmount);
	}

	public void setToAmount(String pToAmount) {
		toAmount.clear();
		toAmount.click();
		toAmount.sendKeys(pToAmount);
	}
	
	public String getTransactionDescription() {
		return descTransaction.getText().trim();
	}
	
	
}
