package com.choucair.formacion.pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class PayBillsPage extends PageObject{
	
	//Tabs buttons
	@FindBy(xpath = "//a[contains(text(),'Pay Bills')]")
	private WebElementFacade btnPayBills;
	
	@FindBy(xpath = "//a[contains(text(),'Pay Saved Payee')]")
	private WebElementFacade tabPaySaved;
	
	@FindBy(xpath = "//a[contains(text(),'Add New Payee')]")
	private WebElementFacade tabAddNewPayee;
	
	@FindBy(xpath = "//a[contains(text(),'Purchase Foreign Currency')]")
	private WebElementFacade tabPurchaseCurrency;

	// Pay Saved Payee
	@FindBy(xpath = "//select[@id='sp_payee']//option")
	private List<WebElement> payeeList;
	
	@FindBy(xpath = "//select[@id='sp_account']//option")
	private List<WebElement> accountList;
	
	@FindBy(id = "sp_amount")
	private WebElementFacade txtSavedAmount;
	
	@FindBy(id = "sp_date")
	private WebElementFacade txtSavedDate;
	
	@FindBy(id = "sp_description")
	private WebElementFacade txtSavedDescription;
	
	@FindBy(id = "pay_saved_payees")
	private WebElementFacade btnPaySavedPayee;
	
	// Add new Payee
	@FindBy(id = "np_new_payee_name")
	private WebElementFacade txtPayeeName;
	
	@FindBy(id = "np_new_payee_address")
	private WebElementFacade txtPayeeAddress;
	
	@FindBy(id = "np_new_payee_account")
	private WebElementFacade txtPayeeAccount;
	
	@FindBy(id = "np_new_payee_details")
	private WebElementFacade txtPayeeDetails;
	
	@FindBy(id = "add_new_payee")
	private WebElementFacade btnAddPayee;
	
	//Purchase Foreign Currency
	@FindBy(xpath = "//select[@id='pc_currency']//option")
	private List<WebElement> currencyList;
	
	@FindBy(id = "pc_amount")
	private WebElementFacade txtAmountCurrency;
	
	@FindBy(id = "pc_inDollars_true")
	private WebElementFacade convertToDollars;
	
	@FindBy(id = "pc_inDollars_false")
	private WebElementFacade convertToSelected;
	
	@FindBy(id = "pc_calculate_costs")
	private WebElementFacade btnCalculateCost;
	
	@FindBy(id = "pc_conversion_amount")
	private WebElementFacade lblCostCalculate;
	
	@FindBy(id = "purchase_cash")
	private WebElementFacade btnPurchase;
	
	// Alert to Succesfully
	@FindBy(xpath = "//button[@class='close']")
	private WebElementFacade alertSuccesfully;
	
	
	public void click_PayBills() {
		btnPayBills.click();
	}
	
	public void select_SavedPayee(String payee) {
		for(WebElement option : payeeList) {
			if (option.getText().contains(payee)) {
				option.click();
				break;
			}
		}
	}
	
	public void select_SavedAccount(String account) {
		for(WebElement option : accountList) {
			if (option.getText().contains(account)) {
				option.click();
				break;
			}
		}
	}
	
	public void setSavedAmount(String amount) {
		txtSavedAmount.clear();
		txtSavedAmount.click();
		txtSavedAmount.sendKeys(amount);
	}
	
	public void setSavedDate(String date) {
		txtSavedDate.clear();
		txtSavedDate.click();
		txtSavedDate.sendKeys(date);
	}
	
	public void setSavedDescription(String description) {
		txtSavedAmount.click();
		txtSavedDescription.clear();
		txtSavedDescription.click();
		txtSavedDescription.sendKeys(description);
	}	
	
	public void click_btnSavedPayeePay() {
		btnPaySavedPayee.click();
	}
	
	public boolean isSuccesfullyTransaction() {
		return alertSuccesfully.isCurrentlyVisible();
	}
	
	public void click_TabNewPayee() {
		tabAddNewPayee.click();
	}
	
	public void setAddPayeeName(String pName) {
		txtPayeeName.clear();
		txtPayeeName.click();
		txtPayeeName.sendKeys(pName);
	}
	
	public void setAddPayeeAddress(String pAddress) {
		txtPayeeAddress.clear();
		txtPayeeAddress.click();
		txtPayeeAddress.sendKeys(pAddress);
	}
	
	public void setAddPayeeAccount(String pAccount) {
		txtPayeeAccount.clear();
		txtPayeeAccount.click();
		txtPayeeAccount.sendKeys(pAccount);
	}
	
	public void setAddPayeeDetails(String pDetail) {
		txtPayeeDetails.clear();
		txtPayeeDetails.click();
		txtPayeeDetails	.sendKeys(pDetail);
	}
	
	public void click_AddNewPayee() {
		btnAddPayee.click();
	}
	
	public void click_TabPurchaseForeignCurrency() {
		tabPurchaseCurrency.click();
	}
	
	public void select_PurchaseCurrency(String currency) {
		for(WebElement option : currencyList) {
			if (option.getText().contains(currency)) {
				option.click();
				break;
			}
		}
	}
	
	public void setPurchaseCurrencyAmount(String amount) {
		txtAmountCurrency.clear();
		txtAmountCurrency.click();
		txtAmountCurrency.sendKeys(amount);
	}
	
	public void click_PurchaseForeignCurrency() {
		btnPurchase.click();
	}
	
	public void click_convertToDollars() {
		convertToDollars.click();
	}
	
}
